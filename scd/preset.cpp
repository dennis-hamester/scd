// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "preset.hpp"
#include "utils.hpp"

using namespace std;

template <typename T>
unique_ptr<T> create_mode(const string& name, const mode_desc_map& modes, const action_desc_map& actions) {
    if(name.empty()) {
        return nullptr;
    }

    auto iter = modes.find(name);
    if(iter != modes.end()) {
        return T::create(iter->second, actions);
    }
    else {
        return nullptr;
    }
}

preset::preset(const preset_desc& desc, const mode_desc_map& modes, const action_desc_map& actions)
    : _name(desc.name) {
    _button_a = create_button_mode(desc.button_a, modes, actions);
    _button_b = create_button_mode(desc.button_b, modes, actions);
    _button_x = create_button_mode(desc.button_x, modes, actions);
    _button_y = create_button_mode(desc.button_y, modes, actions);
    _select = create_button_mode(desc.select, modes, actions);
    _start = create_button_mode(desc.start, modes, actions);
    _steam = create_button_mode(desc.steam, modes, actions);
    _left_shoulder = create_button_mode(desc.left_shoulder, modes, actions);
    _right_shoulder = create_button_mode(desc.right_shoulder, modes, actions);
    _left_gripper = create_button_mode(desc.left_gripper, modes, actions);
    _right_gripper = create_button_mode(desc.right_gripper, modes, actions);
    _left_trigger = create_trigger_mode(desc.left_trigger, modes, actions);
    _right_trigger = create_trigger_mode(desc.right_trigger, modes, actions);
    _joystick = create_joystick_mode(desc.joystick, modes, actions);
    _left_trackpad = create_trackpad_mode(desc.left_trackpad, modes, actions);
    _right_trackpad = create_trackpad_mode(desc.right_trackpad, modes, actions);
}

const std::string& preset::name() const {
    return _name;
}

void preset::update(controller& ctrl, const scraw_controller_state_t& state, uint32_t elapsed) {
    if(_button_a) {
        _button_a->update(ctrl, state.buttons & SCRAW_BTN_A, elapsed);
    }

    if(_button_b) {
        _button_b->update(ctrl, state.buttons & SCRAW_BTN_B, elapsed);
    }

    if(_button_x) {
        _button_x->update(ctrl, state.buttons & SCRAW_BTN_X, elapsed);
    }

    if(_button_y) {
        _button_y->update(ctrl, state.buttons & SCRAW_BTN_Y, elapsed);
    }

    if(_select) {
        _select->update(ctrl, state.buttons & SCRAW_BTN_SELECT, elapsed);
    }

    if(_start) {
        _start->update(ctrl, state.buttons & SCRAW_BTN_START, elapsed);
    }

    if(_steam) {
        _steam->update(ctrl, state.buttons & SCRAW_BTN_STEAM, elapsed);
    }

    if(_left_shoulder) {
        _left_shoulder->update(ctrl, state.buttons & SCRAW_BTN_LEFT_SHOULDER, elapsed);
    }

    if(_right_shoulder) {
        _right_shoulder->update(ctrl, state.buttons & SCRAW_BTN_RIGHT_SHOULDER, elapsed);
    }

    if(_left_gripper) {
        _left_gripper->update(ctrl, state.buttons & SCRAW_BTN_LEFT_GRIPPER, elapsed);
    }

    if(_right_gripper) {
        _right_gripper->update(ctrl, state.buttons & SCRAW_BTN_RIGHT_GRIPPER, elapsed);
    }

    if(_left_trigger) {
        float value = state.left_trigger / 32767.0f;
        _left_trigger->update(ctrl, value, state.buttons & SCRAW_BTN_LEFT_TRIGGER, elapsed);
    }

    if(_right_trigger) {
        float value = state.right_trigger / 32767.0f;
        _right_trigger->update(ctrl, value, state.buttons & SCRAW_BTN_RIGHT_TRIGGER, elapsed);
    }

    if(_joystick) {
        float x = state.joystick_x / (state.joystick_x < 0 ? 32768.0f : 32767.0f);
        float y = state.joystick_y / (state.joystick_y < 0 ? 32768.0f : 32767.0f);
        _joystick->update(ctrl, x, y, state.buttons & SCRAW_BTN_JOYSTICK, elapsed);
    }

    if(_left_trackpad) {
        float x = state.left_trackpad_x / (state.left_trackpad_x < 0 ? 32768.0f : 32767.0f);
        float y = state.left_trackpad_y / (state.left_trackpad_y < 0 ? 32768.0f : 32767.0f);
        float rotation = -20.0f * pi / 180.0f;
        _left_trackpad->update(ctrl, state.buttons & SCRAW_BTN_LEFT_TP_TOUCH, x, y, state.buttons & SCRAW_BTN_LEFT_TP, rotation, elapsed);
    }

    if(_right_trackpad) {
        float x = state.right_trackpad_x / (state.right_trackpad_x < 0 ? 32768.0f : 32767.0f);
        float y = state.right_trackpad_y / (state.right_trackpad_y < 0 ? 32768.0f : 32767.0f);
        float rotation = 20.0f * pi / 180.0f;
        _right_trackpad->update(ctrl, state.buttons & SCRAW_BTN_RIGHT_TP_TOUCH, x, y, state.buttons & SCRAW_BTN_RIGHT_TP, rotation, elapsed);
    }
}

unique_ptr<button_mode> preset::create_button_mode(const string& name, const mode_desc_map& modes, const action_desc_map& actions) {
    return create_mode<button_mode>(name, modes, actions);
}

unique_ptr<trigger_mode> preset::create_trigger_mode(const string& name, const mode_desc_map& modes, const action_desc_map& actions) {
    return create_mode<trigger_mode>(name, modes, actions);
}

unique_ptr<joystick_mode> preset::create_joystick_mode(const string& name, const mode_desc_map& modes, const action_desc_map& actions) {
    return create_mode<joystick_mode>(name, modes, actions);
}

unique_ptr<trackpad_mode> preset::create_trackpad_mode(const string& name, const mode_desc_map& modes, const action_desc_map& actions) {
    return create_mode<trackpad_mode>(name, modes, actions);
}
