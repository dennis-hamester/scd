// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "simple_joystick_mode.hpp"

using namespace std;
using namespace modes::joystick;

simple_joystick_mode::simple_joystick_mode(const mode_desc& desc, const action_desc_map& actions)
    : joystick_mode(desc.name),
      _x(create_axis_actions("x", desc, actions)),
      _y(create_axis_actions("y", desc, actions)),
      _press(create_simple_actions("press", desc, actions)),
      _release(create_simple_actions("release", desc, actions)),
      _feedback(create_simple_actions("feedback", desc, actions)),
      _deadzone(desc.get_as<float>("deadzone")),
      _feedback_grid(desc.get_as<float>("feedback_grid", 10.0f)) {
}

void simple_joystick_mode::update(controller& ctrl, float x, float y, bool pressed, uint32_t elapsed) {
    if(!_deadzone(x, y)) {
        x = 0.0f;
        y = 0.0f;
    }

    if(_feedback_grid > 0.0f) {
        int prev_grid_x = trunc(_prev_x * _feedback_grid);
        int grid_x = trunc(x * _feedback_grid);
        int prev_grid_y = trunc(_prev_y * _feedback_grid);
        int grid_y = trunc(y * _feedback_grid);

        if((prev_grid_x != grid_x) || (prev_grid_y != grid_y)) {
            execute_simple_actions(_feedback, ctrl);
        }
    }

    if(x != _prev_x) {
        execute_axis_actions(_x, ctrl, x);
    }

    if(y != _prev_y) {
        execute_axis_actions(_y, ctrl, y);
    }

    if(!_pressed && pressed) {
        execute_simple_actions(_press, ctrl);
    }
    else if(_pressed && !pressed) {
        execute_simple_actions(_release, ctrl);
    }

    _pressed = pressed;
    _prev_x = x;
    _prev_y = y;
}
