// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "simple_trigger_mode.hpp"

using namespace std;
using namespace modes::trigger;

simple_trigger_mode::simple_trigger_mode(const mode_desc& desc, const action_desc_map& actions)
    : trigger_mode(desc.name),
      _axis(create_axis_actions("axis", desc, actions)),
      _press(create_simple_actions("press", desc, actions)),
      _release(create_simple_actions("release", desc, actions)),
      _feedback(create_simple_actions("feedback", desc, actions)),
      _deadzone(desc.get_as<float>("deadzone")),
      _feedback_grid(desc.get_as<float>("feedback_grid", 3.0f)) {
}

void simple_trigger_mode::update(controller& ctrl, float value, bool pressed, uint32_t elapsed) {
    if(!_deadzone(value)) {
        value = 0.0f;
    }

    if(value != _prev_value) {
        execute_axis_actions(_axis, ctrl, value);
    }

    if(_feedback_grid > 0.0f) {
        int prev_grid_value = trunc(_prev_value * _feedback_grid);
        int grid_value = trunc(value * _feedback_grid);

        if(prev_grid_value != grid_value) {
            execute_simple_actions(_feedback, ctrl);
        }
    }

    if(!_pressed && pressed) {
        execute_simple_actions(_press, ctrl);
    }
    else if(_pressed && !pressed) {
        execute_simple_actions(_release, ctrl);
    }

    _pressed = pressed;
    _prev_value = value;
}
