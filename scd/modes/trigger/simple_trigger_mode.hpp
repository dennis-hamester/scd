// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_MODES_TRIGGER_SIMPLE_TRIGGER_MODE_HPP_
#define _SCD_MODES_TRIGGER_SIMPLE_TRIGGER_MODE_HPP_

#include "../../action.hpp"
#include "../../mode.hpp"
#include "../../utils.hpp"

namespace modes {
namespace trigger {

class simple_trigger_mode
    : public trigger_mode {
public:
    simple_trigger_mode(const mode_desc& desc, const action_desc_map& actions);
    virtual ~simple_trigger_mode() = default;

    virtual void update(controller& ctrl, float value, bool pressed, uint32_t elapsed) override;

private:
    action_list<axis_action> _axis;
    action_list<simple_action> _press;
    action_list<simple_action> _release;
    action_list<simple_action> _feedback;
    deadzone_1d _deadzone;
    float _feedback_grid;
    bool _pressed = false;
    float _prev_value = 0.0f;
};

} // namespace trigger
} // namespace modes

#endif // _SCD_MODES_TRIGGER_SIMPLE_TRIGGER_MODE_HPP_
