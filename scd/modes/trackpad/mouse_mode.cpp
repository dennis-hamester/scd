// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "mouse_mode.hpp"
#include "../../controller.hpp"
#include <cmath>

using namespace std;
using namespace modes::trackpad;

mouse_mode::mouse_mode(const mode_desc& desc, const action_desc_map& actions)
    : trackpad_mode(desc.name),
      _tap(create_simple_actions("tap", desc, actions)),
      _tap_release(create_simple_actions("tap_release", desc, actions)),
      _press(create_simple_actions("press", desc, actions)),
      _release(create_simple_actions("release", desc, actions)),
      _feedback(create_simple_actions("feedback", desc, actions)),
      _deadzone(desc.get_as<float>("radius", 90.0f)),
      _friction((100.0f - desc.get_as<float>("friction", 0.5f)) / 100.0f),
      _sensitivity(desc.get_as<float>("sensitivity", 200.0f)),
      _tap_delay(desc.get_as<int>("tap_delay", 150)),
      _feedback_grid(desc.get_as<float>("feedback_grid", 10.0f)) {
}

void mouse_mode::update(controller& ctrl, bool touched, float x, float y, bool pressed, float rotation, uint32_t elapsed) {
    const float v_threshold = 0.0001;
    const float v_threshold_2 = v_threshold * v_threshold;
    const float v_fake_threshold = 0.001;
    const float v_fake_threshold_2 = v_fake_threshold * v_fake_threshold;

    if(!_touched && touched) {
        _filter_x.reset();
        _filter_y.reset();
    }

    x = _filter_x.filter(x);
    y = _filter_y.filter(y);

    bool in_zone = touched && !_deadzone(x, y);

    // Fake movement if not touched or outside zone
    if(!touched || !in_zone) {
        float vx = _x_axis.get_velocity() / (_sensitivity * 1000.0f);
        float vy = _y_axis.get_velocity() / (_sensitivity * 1000.0f);
        float coeff = pow(_friction, elapsed);

        float v_2 = vx * vx + vy * vy;
        if(v_2 < v_fake_threshold_2) {
            vx = 0.0f;
            vy = 0.0f;
        }

        x = _prev_x + elapsed * coeff * vx;
        y = _prev_y - elapsed * coeff * vy;
    }

    // velocity (per millisecond)
    float dx = (x - _prev_x) / elapsed;
    float dy = (y - _prev_y) / elapsed;
    float d_2 = dx * dx + dy * dy;

    if((!_in_zone && touched) ||
       (elapsed == 0) ||
       (d_2 < v_threshold_2)) {
        dx = 0.0f;
        dy = 0.0f;
        d_2 = 0.0f;
    }

    _x_axis.set_velocity(_sensitivity * 1000.0f * dx);
    _y_axis.set_velocity(-_sensitivity * 1000.0f * dy);

    _x_axis.update(ctrl.get_mouse(), elapsed);
    _y_axis.update(ctrl.get_mouse(), elapsed);

    if(_feedback_grid > 0.0f) {
        int prev_grid_x = trunc(_prev_x * _feedback_grid);
        int grid_x = trunc(x * _feedback_grid);
        int prev_grid_y = trunc(_prev_y * _feedback_grid);
        int grid_y = trunc(y * _feedback_grid);

        if((prev_grid_x != grid_x) || (prev_grid_y != grid_y)) {
            execute_simple_actions(_feedback, ctrl);
        }
    }

    if(pressed && !_pressed) {
        execute_simple_actions(_press, ctrl);
    }
    else if(!pressed && _pressed) {
        execute_simple_actions(_release, ctrl);
    }

    if(!_touched && touched) {
        _cur_tap_dur = 0;
    }
    else if(_touched && !touched) {
        if((_cur_tap_dur > 0) && (_cur_tap_dur <= _tap_delay)) {
            execute_simple_actions(_tap, ctrl);
            execute_simple_actions(_tap_release, ctrl);
        }

        _cur_tap_dur = -1;
    }
    else if(_cur_tap_dur >= 0) {
        if(d_2 < v_fake_threshold_2) {
            _cur_tap_dur += elapsed;
        }
        else {
            _cur_tap_dur = -1;
        }
    }

    _prev_x = x;
    _prev_y = y;
    _in_zone = in_zone;
    _touched = touched;
    _pressed = pressed;
}
