// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_MODES_TRACKPAD_CIRCLE_MODE_HPP_
#define _SCD_MODES_TRACKPAD_CIRCLE_MODE_HPP_

#include "../../action.hpp"
#include "../../mode.hpp"
#include "../../utils.hpp"

namespace modes {
namespace trackpad {

class circle_mode
    : public trackpad_mode {
public:
    circle_mode(const mode_desc& desc, const action_desc_map& actions);
    virtual ~circle_mode() = default;

    virtual void update(controller& ctrl, bool touched, float x, float y, bool pressed, float rotation, uint32_t elapsed) override;

private:
    action_list<simple_action> _cw, _ccw;
    action_list<simple_action> _press, _release;
    float _step;
    deadzone_2d _deadzone, _radius;
    float _prev_x = 0.0f;
    float _prev_y = 0.0f;
    bool _touched = false;
    bool _pressed = false;
    float _prev_arc = 0.0f;
    low_pass_filter _filter_x;
    low_pass_filter _filter_y;
};

} // namespace trackpad
} // namespace modes

#endif // _SCD_MODES_TRACKPAD_CIRCLE_MODE_HPP_
