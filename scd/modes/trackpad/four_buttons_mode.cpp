// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "four_buttons_mode.hpp"
#include "../../utils.hpp"

using namespace std;
using namespace modes::trackpad;

four_buttons_mode::four_buttons_mode(const mode_desc& desc, const action_desc_map& actions)
    : trackpad_mode(desc.name),
      _up(create_simple_actions("up", desc, actions)),
      _up_release(create_simple_actions("up_release", desc, actions)),
      _down(create_simple_actions("down", desc, actions)),
      _down_release(create_simple_actions("down_release", desc, actions)),
      _left(create_simple_actions("left", desc, actions)),
      _left_release(create_simple_actions("left_release", desc, actions)),
      _right(create_simple_actions("right", desc, actions)),
      _right_release(create_simple_actions("right_release", desc, actions)),
      _press(desc.get_as<bool>("press", true)),
      _fan(desc.get_as<float>("fan", 67.5f) * pi / 180.0f),
      _deadzone(desc.get_as<float>("deadzone", 40.0f)) {
}

void four_buttons_mode::update(controller& ctrl, bool touched, float x, float y, bool pressed, float rotation, uint32_t elapsed) {
    if(!_deadzone(x, y)) {
        x = 0.0f;
        y = 0.0f;
        touched = false;
        pressed = false;
    }

    bool active = (_press && pressed) || (!_press && touched);
    float x_p = rotate_vec_x(x, y, rotation);
    float y_p = rotate_vec_y(x, y, rotation);

    float arc = atan2(y_p, x_p);
    if(arc < 0.0f) {
        arc += 2.0f * pi;
    }

    bool up_active = active && angle_in_range(arc, pi / 2.0f, _fan / 2.0f);
    bool down_active = active && angle_in_range(arc, 3.0f * pi / 2.0f, _fan / 2.0f);
    bool left_active = active && angle_in_range(arc, pi, _fan / 2.0f);
    bool right_active = active && angle_in_range(arc, 0, _fan / 2.0f);

    if(!_up_active && up_active) {
        execute_simple_actions(_up, ctrl);
        _up_active = true;
    }
    else if(_up_active && !up_active) {
        execute_simple_actions(_up_release, ctrl);
        _up_active = false;
    }

    if(!_down_active && down_active) {
        execute_simple_actions(_down, ctrl);
        _down_active = true;
    }
    else if(_down_active && !down_active) {
        execute_simple_actions(_down_release, ctrl);
        _down_active = false;
    }

    if(!_left_active && left_active) {
        execute_simple_actions(_left, ctrl);
        _left_active = true;
    }
    else if(_left_active && !left_active) {
        execute_simple_actions(_left_release, ctrl);
        _left_active = false;
    }

    if(!_right_active && right_active) {
        execute_simple_actions(_right, ctrl);
        _right_active = true;
    }
    else if(_right_active && !right_active) {
        execute_simple_actions(_right_release, ctrl);
        _right_active = false;
    }
}
