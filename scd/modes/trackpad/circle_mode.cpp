// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "circle_mode.hpp"
#include "../../controller.hpp"
#include <cmath>

using namespace std;
using namespace modes::trackpad;

circle_mode::circle_mode(const mode_desc& desc, const action_desc_map& actions)
    : trackpad_mode(desc.name),
      _cw(create_simple_actions("clockwise", desc, actions)),
      _ccw(create_simple_actions("counter_clockwise", desc, actions)),
      _press(create_simple_actions("press", desc, actions)),
      _release(create_simple_actions("release", desc, actions)),
      _step(desc.get_as<float>("step", 10.0f) * pi / 180.0f),
      _deadzone(desc.get_as<float>("deadzone", 40)),
      _radius(desc.get_as<float>("radius", 100)) {
}

void circle_mode::update(controller& ctrl, bool touched, float x, float y, bool pressed, float rotation, uint32_t elapsed) {
    float x_p = _filter_x.filter(x);
    float y_p = _filter_y.filter(y);

    if(!_deadzone(x_p, y_p) || _radius(x_p, y_p)) {
        touched = false;
    }

    if(pressed && !_pressed) {
        execute_simple_actions(_press, ctrl);
        _pressed = true;
    }
    else if(!pressed && _pressed) {
        execute_simple_actions(_release, ctrl);
        _pressed = false;
    }

    if(touched) {
        float arc = atan2(y_p, x_p);
        if(!_touched) {
            _prev_arc = arc;
        }

        float diff = angle_diff(_prev_arc, arc);
        int steps = trunc(abs(diff) / _step);
        if(steps > 0) {
            if(diff < 0) {
                for(int i = 0; i < steps; ++i) {
                    execute_simple_actions(_cw, ctrl);
                }

                _prev_arc -= steps * _step;
            }
            else {
                for(int i = 0; i < steps; ++i) {
                    execute_simple_actions(_ccw, ctrl);
                }

                _prev_arc += steps * _step;
            }

            if(_prev_arc > 2.0f * pi) {
                _prev_arc -= 2.0f * pi;
            }
            else if(_prev_arc < 0) {
                _prev_arc += 2.0f * pi;
            }
        }
    }

    _prev_x = x_p;
    _prev_y = y_p;
    _touched = touched;
}
