// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "daemon.hpp"
#include <atomic>
#include <csignal>
#include <fstream>
#include <functional>
#include <iostream>

using namespace std;
namespace fs = boost::filesystem;

atomic_bool running = ATOMIC_VAR_INIT(true);

static void signal_handler(int sig) {
    if(!running.exchange(false)) {
        abort();
    }
}

daemon::daemon(const daemon_options& options)
    : _options(options),
      _scraw_ctx(bind(&daemon::on_controller_gained, this, placeholders::_1), bind(&daemon::on_controller_lost, this, placeholders::_1)) {
    read_configs();
}

daemon::~daemon() {
    for(auto& ctrl : _ctrls) {
        ctrl.second->enable_lizard_mode();
    }
}

void daemon::add_config(const boost::filesystem::path& filename) {
    ifstream is(filename.native());
    if(!is) {
        return;
    }

    controller_config_desc new_desc;

    fs::path ext = filename.extension();
    if(ext == ".toml") {
        new_desc = controller_config_desc::from_toml(is);
    }

    if(!new_desc.name.empty()) {
        _configs.emplace(new_desc.name, move(new_desc));
    }
}

int daemon::run() {
    using clock = chrono::high_resolution_clock;

    _running.store(true);
    _scraw_worker_th = thread(bind(&daemon::scraw_worker, this));
    clock::time_point t_start, t_end;
    clock::duration t_diff(0);
    auto target = chrono::milliseconds(10);

    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    while(running.load()) {
        t_start = chrono::high_resolution_clock::now();

        {
            lock_guard<mutex> lock(_mutex);
            for(auto& ctrl : _ctrls) {
                ctrl.second->update();
            }
        }

        t_end = chrono::high_resolution_clock::now();
        t_diff = t_end - t_start;

        if(target > t_diff) {
            this_thread::sleep_for(target - t_diff);
        }
    }

    _running.store(false);
    _scraw_worker_th.join();

    cout << "\nExiting..." << endl;
    return 0;
}

void daemon::scraw_worker() {
    while(_running.load()) {
        _scraw_ctx.handle_events(500);
    }
}

void daemon::on_controller_gained(scraw::controller scraw_ctrl) {
    lock_guard<mutex> lock(_mutex);
    unique_ptr<controller> new_ctrl = make_unique<controller>(scraw_ctrl);

    const scraw::controller_info& info = new_ctrl->info();
    string ctrl_type;
    switch(info.type) {
    case scraw::controller_type::wired:
        ctrl_type = "wired";
        break;

    case scraw::controller_type::wireless:
        ctrl_type = "wireless";
        break;

    case scraw::controller_type::unknown:
        ctrl_type = "unknown";
        break;
    }

    cout << "Gained new " << ctrl_type << " controller (" << info.serial_number << ")" << endl;

    auto cfg_iter = _configs.find(_options.default_config);
    if(cfg_iter != _configs.end()) {
        new_ctrl->set_config(cfg_iter->second);
    }

    _ctrls.emplace(scraw_ctrl, move(new_ctrl));
}

void daemon::on_controller_lost(scraw::controller scraw_ctrl) {
    lock_guard<mutex> lock(_mutex);

    auto iter = _ctrls.find(scraw_ctrl);
    if(iter != _ctrls.end()) {
        const scraw::controller_info& info = iter->second->info();
        cout << "Lost controller (" << info.serial_number << ")" << endl;
        _ctrls.erase(iter);
    }
}

void daemon::read_configs() {
    if(fs::is_directory(_options.user_configs_path)) {
        for(fs::directory_iterator iter(_options.user_configs_path); iter != fs::directory_iterator(); ++iter) {
            add_config(iter->path());
        }
    }

    if(!_configs.empty()) {
        for(auto& cfg : _configs) {
            cout << "Found configuration " << cfg.first << endl;
        }
    }
    else {
        cout << "No controller configurations were found" << endl;
    }
}
