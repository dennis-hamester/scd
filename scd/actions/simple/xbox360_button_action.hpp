// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_ACTIONS_SIMPLE_XBOX360_BUTTON_ACTION_HPP_
#define _SCD_ACTIONS_SIMPLE_XBOX360_BUTTON_ACTION_HPP_

#include "../../action.hpp"
#include "../../xbox360_controller.hpp"

namespace actions {
namespace simple {

class xbox360_button_action
    : public simple_action {
public:
    xbox360_button_action(const action_desc& desc);
    virtual ~xbox360_button_action() = default;

    virtual void execute(controller& ctrl) override;

private:
    xbox360_button _button;
    bool _state;
};

} // namespace simple
} // namespace actions

#endif // _SCD_ACTIONS_SIMPLE_XBOX360_BUTTON_ACTION_HPP_
