// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "action.hpp"
#include "actions/axis/mouse_axis_action.hpp"
#include "actions/axis/xbox360_axis_action.hpp"
#include "actions/simple/feedback_action.hpp"
#include "actions/simple/keyboard_key_action.hpp"
#include "actions/simple/mouse_axis_action.hpp"
#include "actions/simple/mouse_button_action.hpp"
#include "actions/simple/xbox360_axis_action.hpp"
#include "actions/simple/xbox360_button_action.hpp"

using namespace std;

const string& action::name() const {
    return _name;
}

action::action(const string& name)
    : _name(name) {
}

unique_ptr<simple_action> simple_action::create(const action_desc& desc) {
    const string& type = desc.type;

    if(type == "feedback") {
        return make_unique<actions::simple::feedback_action>(desc);
    }
    else if(type == "keyboard_key") {
        return make_unique<actions::simple::keyboard_key_action>(desc);
    }
    else if(type == "mouse_axis") {
        return make_unique<actions::simple::mouse_axis_action>(desc);
    }
    else if(type == "mouse_button") {
        return make_unique<actions::simple::mouse_button_action>(desc);
    }
    else if(type == "xbox360_axis") {
        return make_unique<actions::simple::xbox360_axis_action>(desc);
    }
    else if(type == "xbox360_button") {
        return make_unique<actions::simple::xbox360_button_action>(desc);
    }

    return nullptr;
}

simple_action::simple_action(const string& name)
    : action(name) {
}

unique_ptr<axis_action> axis_action::create(const action_desc& desc) {
    const string& type = desc.type;

    if(type == "mouse_axis") {
        return make_unique<actions::axis::mouse_axis_action>(desc);
    }
    else if(type == "xbox360_axis") {
        return make_unique<actions::axis::xbox360_axis_action>(desc);
    }

    return nullptr;
}

axis_action::axis_action(const string& name)
    : action(name) {
}
