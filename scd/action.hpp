// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_ACTION_HPP_
#define _SCD_ACTION_HPP_

#include "controller_config_desc.hpp"
#include <memory>
#include <string>
#include <vector>

class controller;

template <typename T>
using action_list = std::vector<std::unique_ptr<T>>;

class action {
public:
    virtual ~action() = default;

    const std::string& name() const;

protected:
    explicit action(const std::string& name);

private:
    std::string _name;
};

class simple_action
    : public action {
public:
    static std::unique_ptr<simple_action> create(const action_desc& desc);

    virtual ~simple_action() = default;
    virtual void execute(controller& ctrl) = 0;

protected:
    explicit simple_action(const std::string& name);
};

class axis_action
    : public action {
public:
    static std::unique_ptr<axis_action> create(const action_desc& desc);

    virtual ~axis_action() = default;
    virtual void execute(controller& ctrl, float value) = 0;

protected:
    explicit axis_action(const std::string& name);
};

#endif // _SCD_ACTION_HPP_
