// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_CONTROLLER_CONFIG_DESC_HPP_
#define _SCD_CONTROLLER_CONFIG_DESC_HPP_

#include <istream>
#include <map>
#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>

namespace boost {
    template <>
    inline bool lexical_cast<bool, std::string>(const std::string& arg) {
        if(arg == "true") {
            return true;
        }
        else if(arg == "false") {
            return false;
        }
        else {
            throw_exception(bad_lexical_cast());
        }
    }
}

namespace detail {
    template <typename T>
    inline T map_get_as(const std::map<std::string, std::string>& map, const std::string& slot, T def) {
        auto iter = map.find(slot);
        if(iter != map.end()) {
            return boost::lexical_cast<T>(iter->second);
        }
        else {
            return def;
        }
    }
}

struct action_desc {
    std::string name;
    std::string type;
    std::map<std::string, std::string> values;

    template <typename T>
    inline T get_as(const std::string& slot, T def = T()) const {
        return detail::map_get_as<T>(values, slot, def);
    }
};

struct mode_desc {
    std::string name;
    std::string type;
    std::map<std::string, std::string> values;
    std::map<std::string, std::vector<std::string>> lists;

    template <typename T>
    inline T get_as(const std::string& slot, T def = T()) const {
        return detail::map_get_as<T>(values, slot, def);
    }
};

struct preset_desc {
    std::string name;
    std::string button_a;
    std::string button_b;
    std::string button_x;
    std::string button_y;
    std::string select;
    std::string start;
    std::string steam;
    std::string left_shoulder;
    std::string right_shoulder;
    std::string left_gripper;
    std::string right_gripper;
    std::string left_trigger;
    std::string right_trigger;
    std::string joystick;
    std::string left_trackpad;
    std::string right_trackpad;
};

using action_desc_map = std::map<std::string, action_desc>;
using mode_desc_map = std::map<std::string, mode_desc>;
using preset_desc_map = std::map<std::string, preset_desc>;

struct controller_config_desc {
    static controller_config_desc from_toml(std::istream& desc);

    std::string name;
    std::string default_preset;
    action_desc_map actions;
    mode_desc_map modes;
    preset_desc_map presets;
};

#endif // _SCD_CONTROLLER_CONFIG_DESC_HPP_
