// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_CONTROLLER_CONFIG_HPP_
#define _SCD_CONTROLLER_CONFIG_HPP_

#include "action.hpp"
#include "mode.hpp"
#include "preset.hpp"
#include "controller_config_desc.hpp"
#include <map>
#include <memory>
#include <string>
#include <scrawpp/controller.hpp>

class controller;

class controller_config {
public:
    controller_config(const controller_config_desc& desc);

    void set_preset(const std::string& name);
    void reset_preset();
    void update(controller& ctrl, const scraw_controller_state_t& state, uint32_t elapsed);

private:
    controller_config_desc _desc;
    std::unique_ptr<preset> _active = nullptr;
};

#endif // _SCD_CONTROLLER_CONFIG_HPP_
