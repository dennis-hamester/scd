// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_MODE_HPP_
#define _SCD_MODE_HPP_

#include "action.hpp"
#include "controller_config_desc.hpp"
#include <memory>
#include <string>

class controller;

class mode {
public:
    virtual ~mode() = default;

    const std::string& name() const;

protected:
    static action_list<simple_action> create_simple_actions(const std::string& slot, const mode_desc& desc, const action_desc_map& actions);
    static action_list<axis_action> create_axis_actions(const std::string& slot, const mode_desc& desc, const action_desc_map& actions);
    static void execute_simple_actions(const action_list<simple_action>& actions, controller& ctrl);
    static void execute_axis_actions(const action_list<axis_action>& actions, controller& ctrl, float value);

    explicit mode(const std::string& name);

private:
    std::string _name;
};

class button_mode
    : public mode {
public:
    static std::unique_ptr<button_mode> create(const mode_desc& desc, const action_desc_map& actions);

    virtual ~button_mode() = default;

    virtual void update(controller& ctrl, bool pressed, uint32_t elapsed) = 0;

protected:
    explicit button_mode(const std::string& name);
};

class trigger_mode
    : public mode {
public:
    static std::unique_ptr<trigger_mode> create(const mode_desc& desc, const action_desc_map& actions);

    virtual ~trigger_mode() = default;

    virtual void update(controller& ctrl, float value, bool pressed, uint32_t elapsed) = 0;

protected:
    explicit trigger_mode(const std::string& name);
};

class joystick_mode
    : public mode {
public:
    static std::unique_ptr<joystick_mode> create(const mode_desc& desc, const action_desc_map& actions);

    virtual ~joystick_mode() = default;

    virtual void update(controller& ctrl, float x, float y, bool pressed, uint32_t elapsed) = 0;

protected:
    explicit joystick_mode(const std::string& name);
};

class trackpad_mode
    : public mode {
public:
    static std::unique_ptr<trackpad_mode> create(const mode_desc& desc, const action_desc_map& actions);

    virtual ~trackpad_mode() = default;

    virtual void update(controller& ctrl, bool touched, float x, float y, bool pressed, float rotation, uint32_t elapsed) = 0;

protected:
    explicit trackpad_mode(const std::string& name);
};

#endif // _SCD_MODE_HPP_
