// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_PRESET_HPP_
#define _SCD_PRESET_HPP_

#include "controller_config_desc.hpp"
#include "mode.hpp"
#include <memory>
#include <string>
#include <scrawpp/controller.hpp>

class controller;

class preset {
public:
    preset(const preset_desc& desc, const mode_desc_map& modes, const action_desc_map& actions);

    const std::string& name() const;
    void update(controller& ctrl, const scraw_controller_state_t& state, uint32_t elapsed);

private:
    static std::unique_ptr<button_mode> create_button_mode(const std::string& name, const mode_desc_map& modes, const action_desc_map& actions);
    static std::unique_ptr<trigger_mode> create_trigger_mode(const std::string& name, const mode_desc_map& modes, const action_desc_map& actions);
    static std::unique_ptr<joystick_mode> create_joystick_mode(const std::string& name, const mode_desc_map& modes, const action_desc_map& actions);
    static std::unique_ptr<trackpad_mode> create_trackpad_mode(const std::string& name, const mode_desc_map& modes, const action_desc_map& actions);

    std::string _name;
    std::unique_ptr<button_mode> _button_a;
    std::unique_ptr<button_mode> _button_b;
    std::unique_ptr<button_mode> _button_x;
    std::unique_ptr<button_mode> _button_y;
    std::unique_ptr<button_mode> _select;
    std::unique_ptr<button_mode> _start;
    std::unique_ptr<button_mode> _steam;
    std::unique_ptr<button_mode> _left_shoulder;
    std::unique_ptr<button_mode> _right_shoulder;
    std::unique_ptr<button_mode> _left_gripper;
    std::unique_ptr<button_mode> _right_gripper;
    std::unique_ptr<trigger_mode> _left_trigger;
    std::unique_ptr<trigger_mode> _right_trigger;
    std::unique_ptr<joystick_mode> _joystick;
    std::unique_ptr<trackpad_mode> _left_trackpad;
    std::unique_ptr<trackpad_mode> _right_trackpad;
};

#endif // _SCD_PRESET_HPP_
