// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_DAEMON_HPP_
#define _SCD_DAEMON_HPP_

#include "controller_config_desc.hpp"
#include "controller.hpp"
#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <boost/filesystem.hpp>
#include <scrawpp/context.hpp>
#include <scrawpp/controller.hpp>

struct daemon_options {
    boost::filesystem::path user_configs_path;
    std::string default_config = "default";
};

class daemon {
public:
    explicit daemon(const daemon_options& options);
    ~daemon();

    void add_config(const boost::filesystem::path& filename);
    int run();

private:
    void scraw_worker();
    void on_controller_gained(scraw::controller scraw_ctrl);
    void on_controller_lost(scraw::controller scraw_ctrl);

    void read_configs();

    std::mutex _mutex;
    daemon_options _options;
    std::thread _scraw_worker_th;
    std::atomic_bool _running;
    std::map<std::string, controller_config_desc> _configs;
    std::unordered_map<scraw::controller, std::unique_ptr<controller>> _ctrls;
    scraw::context _scraw_ctx;
};

#endif // _SCD_DAEMON_HPP_
