// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "mouse.hpp"
#include <map>

using namespace std;

static const map<string, mouse_button> mouse_button_name_map = {
    { "left",    mouse_button::btn_left },
    { "right",   mouse_button::btn_right },
    { "middle",  mouse_button::btn_middle },
    { "forward", mouse_button::btn_forward },
    { "back",    mouse_button::btn_back },
};

static const map<string, mouse_axis> mouse_axis_name_map = {
    { "x",       mouse_axis::axis_x },
    { "y",       mouse_axis::axis_y },
    { "wheel_v", mouse_axis::axis_wheel_v },
    { "wheel_h", mouse_axis::axis_wheel_h },
};

mouse_button mouse_button_from_string(const string& name) {
    return mouse_button_name_map.at(name);
}

mouse_axis mouse_axis_from_string(const string& name) {
    return mouse_axis_name_map.at(name);
}
