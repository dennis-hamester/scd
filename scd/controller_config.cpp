// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "controller_config.hpp"

using namespace std;

controller_config::controller_config(const controller_config_desc& desc)
    : _desc(desc) {
    if(!_desc.default_preset.empty()) {
        set_preset(_desc.default_preset);
    }
}

void controller_config::set_preset(const std::string& name) {
    auto iter = _desc.presets.find(name);
    if(iter == _desc.presets.end()) {
        return;
    }

    _active = make_unique<preset>(iter->second, _desc.modes, _desc.actions);
}

void controller_config::reset_preset() {
    _active = nullptr;
}

void controller_config::update(controller& ctrl, const scraw_controller_state_t& state, uint32_t elapsed) {
    if(_active) {
        _active->update(ctrl, state, elapsed);
    }
}
