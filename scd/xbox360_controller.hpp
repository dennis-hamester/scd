// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_XBOX360_CONTROLLER_HPP_
#define _SCD_XBOX360_CONTROLLER_HPP_

#include <scd/config.hpp>
#include <string>

enum class xbox360_button {
    btn_a = 0,
    btn_b,
    btn_x,
    btn_y,
    btn_left_shoulder,
    btn_right_shoulder,
    btn_select,
    btn_start,
    btn_mode,
    btn_left_thumb,
    btn_right_thumb,

    btn_max // Must be last
};

enum class xbox360_axis {
    axis_left_thumb_x = 0,
    axis_left_thumb_y,
    axis_right_thumb_x,
    axis_right_thumb_y,
    axis_left_trigger,
    axis_right_trigger,
    axis_dpad_x,
    axis_dpad_y,

    axis_max // Must be last
};

xbox360_button xbox360_button_from_string(const std::string& name);
xbox360_axis xbox360_axis_from_string(const std::string& name);

#if defined(SCD_OS_LINUX)
#include "linux/xbox360_controller.hpp"
#endif

#endif // _SCD_XBOX360_CONTROLLER_HPP_
