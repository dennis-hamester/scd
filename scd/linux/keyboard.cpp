// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "keyboard.hpp"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <sys/ioctl.h>
#include <unistd.h>

using namespace std;

keyboard::keyboard() {
    add_keys();

    uinput_user_dev dev;
    memset(&dev, 0, sizeof(uinput_user_dev));

    _fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(_fd < 0) {
        cerr << "Failed to open /dev/uinput" << endl;
        return;
    }

    ioctl(_fd, UI_SET_EVBIT, EV_SYN);

    ioctl(_fd, UI_SET_EVBIT, EV_KEY);
    for(auto& k : _keys) {
        ioctl(_fd, UI_SET_KEYBIT, k.ev_key);
    }

    snprintf(dev.name, UINPUT_MAX_NAME_SIZE, "scd keyboard");
    dev.id.bustype = BUS_VIRTUAL;
    if(write(_fd, &dev, sizeof(uinput_user_dev)) != sizeof(uinput_user_dev)) {
        cerr << "Failed to create device \"" << dev.name << "\"" << endl;
        close(_fd);
        _fd = -1;
        return;
    }

    ioctl(_fd, UI_DEV_CREATE);
}

keyboard::~keyboard() {
    if(_fd >= 0) {
        ioctl(_fd, UI_DEV_DESTROY);
        close(_fd);
    }
}

void keyboard::press(keyboard_key key) {
    if(_fd < 0) {
        return;
    }

    auto& key_ = _keys[(size_t)key];
    ++key_.count;

    if(key_.count == 1) {
        input_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.type = EV_KEY;
        ev.code = key_.ev_key;
        ev.value = 1;

        if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            cerr << "Failed to write to /dev/uinput" << endl;
        }

        _need_sync = true;
    }
}

void keyboard::release(keyboard_key key) {
    if(_fd < 0) {
        return;
    }

    auto& key_ = _keys[(size_t)key];
    --key_.count;

    if(key_.count == 0) {
        input_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.type = EV_KEY;
        ev.code = key_.ev_key;
        ev.value = 0;

        if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            cerr << "Failed to write to /dev/uinput" << endl;
        }

        _need_sync = true;
    }
}

void keyboard::sync() {
    if(!_need_sync || (_fd < 0)) {
        return;
    }

    input_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = EV_SYN;

    if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
        cerr << "Failed to write to /dev/uinput" << endl;
    }

    _need_sync = false;
}

void keyboard::add_keys() {
    _keys.clear();
    _keys.resize((size_t)keyboard_key::key_max);

    _keys[(size_t)keyboard_key::key_esc].ev_key = KEY_ESC;
    _keys[(size_t)keyboard_key::key_1].ev_key = KEY_1;
    _keys[(size_t)keyboard_key::key_2].ev_key = KEY_2;
    _keys[(size_t)keyboard_key::key_3].ev_key = KEY_3;
    _keys[(size_t)keyboard_key::key_4].ev_key = KEY_4;
    _keys[(size_t)keyboard_key::key_5].ev_key = KEY_5;
    _keys[(size_t)keyboard_key::key_6].ev_key = KEY_6;
    _keys[(size_t)keyboard_key::key_7].ev_key = KEY_7;
    _keys[(size_t)keyboard_key::key_8].ev_key = KEY_8;
    _keys[(size_t)keyboard_key::key_9].ev_key = KEY_9;
    _keys[(size_t)keyboard_key::key_0].ev_key = KEY_0;
    _keys[(size_t)keyboard_key::key_minus].ev_key = KEY_MINUS;
    _keys[(size_t)keyboard_key::key_equal].ev_key = KEY_EQUAL;
    _keys[(size_t)keyboard_key::key_backspace].ev_key = KEY_BACKSPACE;
    _keys[(size_t)keyboard_key::key_tab].ev_key = KEY_TAB;
    _keys[(size_t)keyboard_key::key_q].ev_key = KEY_Q;
    _keys[(size_t)keyboard_key::key_w].ev_key = KEY_W;
    _keys[(size_t)keyboard_key::key_e].ev_key = KEY_E;
    _keys[(size_t)keyboard_key::key_r].ev_key = KEY_R;
    _keys[(size_t)keyboard_key::key_t].ev_key = KEY_T;
    _keys[(size_t)keyboard_key::key_y].ev_key = KEY_Y;
    _keys[(size_t)keyboard_key::key_u].ev_key = KEY_U;
    _keys[(size_t)keyboard_key::key_i].ev_key = KEY_I;
    _keys[(size_t)keyboard_key::key_o].ev_key = KEY_O;
    _keys[(size_t)keyboard_key::key_p].ev_key = KEY_P;
    _keys[(size_t)keyboard_key::key_leftbrace].ev_key = KEY_LEFTBRACE;
    _keys[(size_t)keyboard_key::key_rightbrace].ev_key = KEY_RIGHTBRACE;
    _keys[(size_t)keyboard_key::key_enter].ev_key = KEY_ENTER;
    _keys[(size_t)keyboard_key::key_leftctrl].ev_key = KEY_LEFTCTRL;
    _keys[(size_t)keyboard_key::key_a].ev_key = KEY_A;
    _keys[(size_t)keyboard_key::key_s].ev_key = KEY_S;
    _keys[(size_t)keyboard_key::key_d].ev_key = KEY_D;
    _keys[(size_t)keyboard_key::key_f].ev_key = KEY_F;
    _keys[(size_t)keyboard_key::key_g].ev_key = KEY_G;
    _keys[(size_t)keyboard_key::key_h].ev_key = KEY_H;
    _keys[(size_t)keyboard_key::key_j].ev_key = KEY_J;
    _keys[(size_t)keyboard_key::key_k].ev_key = KEY_K;
    _keys[(size_t)keyboard_key::key_l].ev_key = KEY_L;
    _keys[(size_t)keyboard_key::key_semicolon].ev_key = KEY_SEMICOLON;
    _keys[(size_t)keyboard_key::key_apostrophe].ev_key = KEY_APOSTROPHE;
    _keys[(size_t)keyboard_key::key_grave].ev_key = KEY_GRAVE;
    _keys[(size_t)keyboard_key::key_leftshift].ev_key = KEY_LEFTSHIFT;
    _keys[(size_t)keyboard_key::key_backslash].ev_key = KEY_BACKSLASH;
    _keys[(size_t)keyboard_key::key_z].ev_key = KEY_Z;
    _keys[(size_t)keyboard_key::key_x].ev_key = KEY_X;
    _keys[(size_t)keyboard_key::key_c].ev_key = KEY_C;
    _keys[(size_t)keyboard_key::key_v].ev_key = KEY_V;
    _keys[(size_t)keyboard_key::key_b].ev_key = KEY_B;
    _keys[(size_t)keyboard_key::key_n].ev_key = KEY_N;
    _keys[(size_t)keyboard_key::key_m].ev_key = KEY_M;
    _keys[(size_t)keyboard_key::key_comma].ev_key = KEY_COMMA;
    _keys[(size_t)keyboard_key::key_dot].ev_key = KEY_DOT;
    _keys[(size_t)keyboard_key::key_slash].ev_key = KEY_SLASH;
    _keys[(size_t)keyboard_key::key_rightshift].ev_key = KEY_RIGHTSHIFT;
    _keys[(size_t)keyboard_key::key_kpasterisk].ev_key = KEY_KPASTERISK;
    _keys[(size_t)keyboard_key::key_leftalt].ev_key = KEY_LEFTALT;
    _keys[(size_t)keyboard_key::key_space].ev_key = KEY_SPACE;
    _keys[(size_t)keyboard_key::key_capslock].ev_key = KEY_CAPSLOCK;
    _keys[(size_t)keyboard_key::key_f1].ev_key = KEY_F1;
    _keys[(size_t)keyboard_key::key_f2].ev_key = KEY_F2;
    _keys[(size_t)keyboard_key::key_f3].ev_key = KEY_F3;
    _keys[(size_t)keyboard_key::key_f4].ev_key = KEY_F4;
    _keys[(size_t)keyboard_key::key_f5].ev_key = KEY_F5;
    _keys[(size_t)keyboard_key::key_f6].ev_key = KEY_F6;
    _keys[(size_t)keyboard_key::key_f7].ev_key = KEY_F7;
    _keys[(size_t)keyboard_key::key_f8].ev_key = KEY_F8;
    _keys[(size_t)keyboard_key::key_f9].ev_key = KEY_F9;
    _keys[(size_t)keyboard_key::key_f10].ev_key = KEY_F10;
    _keys[(size_t)keyboard_key::key_numlock].ev_key = KEY_NUMLOCK;
    _keys[(size_t)keyboard_key::key_scrolllock].ev_key = KEY_SCROLLLOCK;
    _keys[(size_t)keyboard_key::key_kp7].ev_key = KEY_KP7;
    _keys[(size_t)keyboard_key::key_kp8].ev_key = KEY_KP8;
    _keys[(size_t)keyboard_key::key_kp9].ev_key = KEY_KP9;
    _keys[(size_t)keyboard_key::key_kpminus].ev_key = KEY_KPMINUS;
    _keys[(size_t)keyboard_key::key_kp4].ev_key = KEY_KP4;
    _keys[(size_t)keyboard_key::key_kp5].ev_key = KEY_KP5;
    _keys[(size_t)keyboard_key::key_kp6].ev_key = KEY_KP6;
    _keys[(size_t)keyboard_key::key_kpplus].ev_key = KEY_KPPLUS;
    _keys[(size_t)keyboard_key::key_kp1].ev_key = KEY_KP1;
    _keys[(size_t)keyboard_key::key_kp2].ev_key = KEY_KP2;
    _keys[(size_t)keyboard_key::key_kp3].ev_key = KEY_KP3;
    _keys[(size_t)keyboard_key::key_kp0].ev_key = KEY_KP0;
    _keys[(size_t)keyboard_key::key_kpdot].ev_key = KEY_KPDOT;
    _keys[(size_t)keyboard_key::key_f11].ev_key = KEY_F11;
    _keys[(size_t)keyboard_key::key_f12].ev_key = KEY_F12;
    _keys[(size_t)keyboard_key::key_kpenter].ev_key = KEY_KPENTER;
    _keys[(size_t)keyboard_key::key_rightctrl].ev_key = KEY_RIGHTCTRL;
    _keys[(size_t)keyboard_key::key_kpslash].ev_key = KEY_KPSLASH;
    _keys[(size_t)keyboard_key::key_sysrq].ev_key = KEY_SYSRQ;
    _keys[(size_t)keyboard_key::key_rightalt].ev_key = KEY_RIGHTALT;
    _keys[(size_t)keyboard_key::key_home].ev_key = KEY_HOME;
    _keys[(size_t)keyboard_key::key_up].ev_key = KEY_UP;
    _keys[(size_t)keyboard_key::key_pageup].ev_key = KEY_PAGEUP;
    _keys[(size_t)keyboard_key::key_left].ev_key = KEY_LEFT;
    _keys[(size_t)keyboard_key::key_right].ev_key = KEY_RIGHT;
    _keys[(size_t)keyboard_key::key_end].ev_key = KEY_END;
    _keys[(size_t)keyboard_key::key_down].ev_key = KEY_DOWN;
    _keys[(size_t)keyboard_key::key_pagedown].ev_key = KEY_PAGEDOWN;
    _keys[(size_t)keyboard_key::key_insert].ev_key = KEY_INSERT;
    _keys[(size_t)keyboard_key::key_delete].ev_key = KEY_DELETE;
    _keys[(size_t)keyboard_key::key_pause].ev_key = KEY_PAUSE;
    _keys[(size_t)keyboard_key::key_kpcomma].ev_key = KEY_KPCOMMA;
    _keys[(size_t)keyboard_key::key_leftmeta].ev_key = KEY_LEFTMETA;
    _keys[(size_t)keyboard_key::key_rightmeta].ev_key = KEY_RIGHTMETA;
    _keys[(size_t)keyboard_key::key_compose].ev_key = KEY_COMPOSE;
}
