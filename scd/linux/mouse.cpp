// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "mouse.hpp"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <sys/ioctl.h>
#include <unistd.h>

using namespace std;

mouse::mouse() {
    add_buttons();
    add_axes();

    uinput_user_dev dev;
    memset(&dev, 0, sizeof(uinput_user_dev));

    _fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(_fd < 0) {
        cerr << "Failed to open /dev/uinput" << endl;
        return;
    }

    ioctl(_fd, UI_SET_EVBIT, EV_SYN);

    ioctl(_fd, UI_SET_EVBIT, EV_KEY);
    for(auto& b : _buttons) {
        ioctl(_fd, UI_SET_KEYBIT, b.ev_btn);
    }

    ioctl(_fd, UI_SET_EVBIT, EV_REL);
    for(auto& a : _axes) {
        ioctl(_fd, UI_SET_RELBIT, a.ev_axis);
    }

    snprintf(dev.name, UINPUT_MAX_NAME_SIZE, "scd mouse");
    dev.id.bustype = BUS_VIRTUAL;
    if(write(_fd, &dev, sizeof(uinput_user_dev)) != sizeof(uinput_user_dev)) {
        cerr << "Failed to create device \"" << dev.name << "\"" << endl;
        close(_fd);
        _fd = -1;
        return;
    }

    ioctl(_fd, UI_DEV_CREATE);
}

mouse::~mouse() {
    if(_fd >= 0) {
        ioctl(_fd, UI_DEV_DESTROY);
        close(_fd);
    }
}

void mouse::press(mouse_button btn) {
    if(_fd < 0) {
        return;
    }

    auto& btn_ = _buttons[(size_t)btn];
    ++btn_.count;

    if(btn_.count == 1) {
        input_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.type = EV_KEY;
        ev.code = btn_.ev_btn;
        ev.value = 1;

        if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            cerr << "Failed to write to /dev/uinput" << endl;
        }

        _need_sync = true;
    }
}

void mouse::release(mouse_button btn) {
    if(_fd < 0) {
        return;
    }

    auto& btn_ = _buttons[(size_t)btn];
    --btn_.count;

    if(btn_.count == 0) {
        input_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.type = EV_KEY;
        ev.code = btn_.ev_btn;
        ev.value = 0;

        if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            cerr << "Failed to write to /dev/uinput" << endl;
        }

        _need_sync = true;
    }
}

void mouse::move(mouse_axis axis, float value) {
    if(_fd < 0) {
        return;
    }

    auto& axis_ = _axes[(size_t)axis];
    input_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = EV_REL;
    ev.code = axis_.ev_axis;
    ev.value = value;

    if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
        cerr << "Failed to write to /dev/uinput" << endl;
    }

    _need_sync = true;
}

void mouse::sync() {
    if(!_need_sync || (_fd < 0)) {
        return;
    }

    input_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = EV_SYN;

    if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
        cerr << "Failed to write to /dev/uinput" << endl;
    }

    _need_sync = false;
}

void mouse::add_buttons() {
    _buttons.clear();
    _buttons.resize((size_t)mouse_button::btn_max);

    _buttons[(size_t)mouse_button::btn_left].ev_btn = BTN_LEFT;
    _buttons[(size_t)mouse_button::btn_right].ev_btn = BTN_RIGHT;
    _buttons[(size_t)mouse_button::btn_middle].ev_btn = BTN_MIDDLE;
    _buttons[(size_t)mouse_button::btn_forward].ev_btn = BTN_EXTRA;
    _buttons[(size_t)mouse_button::btn_back].ev_btn = BTN_SIDE;
}

void mouse::add_axes() {
    _axes.clear();
    _axes.resize((size_t)mouse_axis::axis_max);

    _axes[(size_t)mouse_axis::axis_x].ev_axis = REL_X;
    _axes[(size_t)mouse_axis::axis_y].ev_axis = REL_Y;
    _axes[(size_t)mouse_axis::axis_wheel_v].ev_axis = REL_WHEEL;
    _axes[(size_t)mouse_axis::axis_wheel_h].ev_axis = REL_HWHEEL;
}
