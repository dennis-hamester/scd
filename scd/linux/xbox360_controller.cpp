// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "xbox360_controller.hpp"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <sys/ioctl.h>
#include <unistd.h>

using namespace std;

xbox360_controller::xbox360_controller() {
    add_buttons();
    add_axes();

    uinput_user_dev dev;
    memset(&dev, 0, sizeof(uinput_user_dev));

    _fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if(_fd < 0) {
        cerr << "Failed to open /dev/uinput" << endl;
        return;
    }

    ioctl(_fd, UI_SET_EVBIT, EV_SYN);

    ioctl(_fd, UI_SET_EVBIT, EV_KEY);
    for(auto& b : _buttons) {
        ioctl(_fd, UI_SET_KEYBIT, b.ev_btn);
    }

    ioctl(_fd, UI_SET_EVBIT, EV_ABS);
    for(auto& a : _axes) {
        ioctl(_fd, UI_SET_ABSBIT, a.ev_axis);
        dev.absmin[a.ev_axis] = a.min;
        dev.absmax[a.ev_axis] = a.max;
    }

    snprintf(dev.name, UINPUT_MAX_NAME_SIZE, "scd X-Box 360 controller");
    dev.id.bustype = BUS_USB;
    dev.id.vendor = 0x045e;
    dev.id.product = 0x028e;
    dev.id.version = 0x0114;
    if(write(_fd, &dev, sizeof(uinput_user_dev)) != sizeof(uinput_user_dev)) {
        cerr << "Failed to create device \"" << dev.name << "\"" << endl;
        close(_fd);
        _fd = -1;
        return;
    }

    ioctl(_fd, UI_DEV_CREATE);
}

xbox360_controller::~xbox360_controller() {
    if(_fd >= 0) {
        ioctl(_fd, UI_DEV_DESTROY);
        close(_fd);
    }
}

void xbox360_controller::press(xbox360_button btn) {
    if(_fd < 0) {
        return;
    }

    auto& btn_ = _buttons[(size_t)btn];
    ++btn_.count;

    if(btn_.count == 1) {
        input_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.type = EV_KEY;
        ev.code = btn_.ev_btn;
        ev.value = 1;

        if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            cerr << "Failed to write to /dev/uinput" << endl;
        }

        _need_sync = true;
    }
}

void xbox360_controller::release(xbox360_button btn) {
    if(_fd < 0) {
        return;
    }

    auto& btn_ = _buttons[(size_t)btn];
    --btn_.count;

    if(btn_.count == 0) {
        input_event ev;
        memset(&ev, 0, sizeof(ev));
        ev.type = EV_KEY;
        ev.code = btn_.ev_btn;
        ev.value = 0;

        if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            cerr << "Failed to write to /dev/uinput" << endl;
        }

        _need_sync = true;
    }
}

void xbox360_controller::move(xbox360_axis axis, float value) {
    if(_fd < 0) {
        return;
    }

    auto& axis_ = _axes[(size_t)axis];

    if(value > 0.0f) {
        value *= axis_.max;
    }
    else {
        value *= -axis_.min;
    }

    input_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = EV_ABS;
    ev.code = axis_.ev_axis;
    ev.value = value;

    if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
        cerr << "Failed to write to /dev/uinput" << endl;
    }

    _need_sync = true;
}

void xbox360_controller::sync() {
    if(!_need_sync || (_fd < 0)) {
        return;
    }

    input_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = EV_SYN;

    if(write(_fd, &ev, sizeof(ev)) != sizeof(ev)) {
        cerr << "Failed to write to /dev/uinput" << endl;
    }

    _need_sync = false;
}

void xbox360_controller::add_buttons() {
    _buttons.clear();
    _buttons.resize((size_t)xbox360_button::btn_max);

    _buttons[(size_t)xbox360_button::btn_a].ev_btn = BTN_SOUTH;
    _buttons[(size_t)xbox360_button::btn_b].ev_btn = BTN_EAST;
    _buttons[(size_t)xbox360_button::btn_x].ev_btn = BTN_NORTH;
    _buttons[(size_t)xbox360_button::btn_y].ev_btn = BTN_WEST;
    _buttons[(size_t)xbox360_button::btn_left_shoulder].ev_btn = BTN_TL;
    _buttons[(size_t)xbox360_button::btn_right_shoulder].ev_btn = BTN_TR;
    _buttons[(size_t)xbox360_button::btn_select].ev_btn = BTN_SELECT;
    _buttons[(size_t)xbox360_button::btn_start].ev_btn = BTN_START;
    _buttons[(size_t)xbox360_button::btn_mode].ev_btn = BTN_MODE;
    _buttons[(size_t)xbox360_button::btn_left_thumb].ev_btn = BTN_THUMBL;
    _buttons[(size_t)xbox360_button::btn_right_thumb].ev_btn = BTN_THUMBR;
}

void xbox360_controller::add_axes() {
    _axes.clear();
    _axes.resize((size_t)xbox360_axis::axis_max);

    _axes[(size_t)xbox360_axis::axis_left_thumb_x] = { ABS_X, -32768, 32767 };
    _axes[(size_t)xbox360_axis::axis_left_thumb_y] = { ABS_Y, -32768, 32767 };
    _axes[(size_t)xbox360_axis::axis_right_thumb_x] = { ABS_RX, -32768, 32767 };
    _axes[(size_t)xbox360_axis::axis_right_thumb_y] = { ABS_RY, -32768, 32767 };
    _axes[(size_t)xbox360_axis::axis_left_trigger] = { ABS_Z, 0, 255 };
    _axes[(size_t)xbox360_axis::axis_right_trigger] = { ABS_RZ, 0, 255 };
    _axes[(size_t)xbox360_axis::axis_dpad_x] = { ABS_HAT0X, -1, 1 };
    _axes[(size_t)xbox360_axis::axis_dpad_y] = { ABS_HAT0Y, -1, 1 };
}
