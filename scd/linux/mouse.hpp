// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_LINUX_MOUSE_HPP_
#define _SCD_LINUX_MOUSE_HPP_

#include "../mouse.hpp"
#include <vector>

class mouse {
public:
    mouse();
    ~mouse();

    void press(mouse_button btn);
    void release(mouse_button btn);
    void move(mouse_axis axis, float value);
    void sync();

private:
    struct button {
        uint16_t ev_btn;
        int count = 0;
    };

    struct axis {
        uint16_t ev_axis;
    };

    void add_buttons();
    void add_axes();

    int _fd;
    std::vector<button> _buttons;
    std::vector<axis> _axes;
    bool _need_sync = false;
};

#endif // _SCD_LINUX_MOUSE_HPP_
