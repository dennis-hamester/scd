// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_CONTROLLER_HPP_
#define _SCD_CONTROLLER_HPP_

#include "controller_config.hpp"
#include "controller_config_desc.hpp"
#include "keyboard.hpp"
#include "mouse.hpp"
#include "xbox360_controller.hpp"
#include <chrono>
#include <memory>
#include <mutex>
#include <scrawpp/controller.hpp>

class controller {
public:
    explicit controller(scraw::controller scraw_ctrl);

    const scraw::controller_info& info() const;
    void update();
    void enable_lizard_mode();
    void feedback(scraw::feedback_side side, uint16_t amplitude, uint16_t perid, uint16_t count);
    void set_config(const controller_config_desc& desc);
    void clear_config();
    keyboard& get_keyboard();
    mouse& get_mouse();
    xbox360_controller& get_xbox360_controller();

private:
    using clock = std::chrono::steady_clock;

    void on_state_change(const scraw_controller_state_t& state);

    std::mutex _mutex;
    scraw::controller _scraw_ctrl;
    scraw::controller_info _info;
    scraw_controller_state_t _state = { 0 };
    clock::time_point _last_update = clock::now();
    std::unique_ptr<controller_config> _config = nullptr;
    keyboard _keyboard;
    mouse _mouse;
    xbox360_controller _xbox360;
};

#endif // _SCD_CONTROLLER_HPP_
