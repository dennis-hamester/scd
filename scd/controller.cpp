// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "controller.hpp"
#include <iostream>

using namespace std;

controller::controller(scraw::controller scraw_ctrl)
    : _scraw_ctrl(move(scraw_ctrl)) {
    scraw::controller_config ctrl_cfg;
    ctrl_cfg.idle_timeout = 300;
    ctrl_cfg.imu = false;
    _scraw_ctrl.configure(ctrl_cfg);
    _scraw_ctrl.lizard_buttons(false);
    _scraw_ctrl.on_state_change() = bind(&controller::on_state_change, this, placeholders::_2);
    _info = _scraw_ctrl.info();
}

const scraw::controller_info& controller::info() const {
    return _info;
}

void controller::update() {
    lock_guard<mutex> lock(_mutex);

    auto now = clock::now();
    auto elapsed = chrono::duration_cast<chrono::milliseconds>(now - _last_update).count();
    _last_update = now;

    if(_config) {
        _config->update(*this, _state, elapsed);
        _keyboard.sync();
        _mouse.sync();
        _xbox360.sync();
    }
}

void controller::enable_lizard_mode() {
    _scraw_ctrl.lizard_buttons(true);
    _scraw_ctrl.enable_lizard_analog();
}

void controller::feedback(scraw::feedback_side side, uint16_t amplitude, uint16_t perid, uint16_t count) {
    _scraw_ctrl.feedback(side, amplitude, perid, count);
}

void controller::set_config(const controller_config_desc& desc) {
    _config = make_unique<controller_config>(desc);
    cout << "Controller " << _info.serial_number << ": set configuration to " << desc.name << endl;
}

void controller::clear_config() {
    _config = nullptr;
}

keyboard& controller::get_keyboard() {
    return _keyboard;
}

mouse& controller::get_mouse() {
    return _mouse;
}

xbox360_controller& controller::get_xbox360_controller() {
    return _xbox360;
}

void controller::on_state_change(const scraw_controller_state_t& state) {
    lock_guard<mutex> lock(_mutex);
    _state = state;
}
