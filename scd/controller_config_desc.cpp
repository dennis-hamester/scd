// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "controller_config_desc.hpp"
#include <tinytoml/toml.h>

using namespace std;

controller_config_desc controller_config_desc::from_toml(istream& desc) {
    toml::ParseResult pr = toml::parse(desc);
    const toml::Value& root = pr.value;
    controller_config_desc res;

    const toml::Value* sec_cfg = root.find("config");
    if(sec_cfg) {
        const toml::Value* name = sec_cfg->find("name");
        if(name) {
            res.name = name->as<string>();
        }

        const toml::Value* default_preset = sec_cfg->find("default_preset");
        if(default_preset) {
            res.default_preset = default_preset->as<string>();
        }
    }

    const toml::Value* sec_actions = root.find("actions");
    if(sec_actions) {
        const toml::Table& actions = sec_actions->as<toml::Table>();
        for(auto& action : actions) {
            action_desc new_action;
            new_action.name = action.first;

            for(auto& key_val : action.second.as<toml::Table>()) {
                const string& key = key_val.first;
                const string& val = key_val.second.as<string>();

                if(key == "type") {
                    new_action.type = val;
                }
                else {
                    new_action.values[key] = val;
                }
            }

            if(!new_action.type.empty()) {
                res.actions[new_action.name] = move(new_action);
            }
        }
    }


    const toml::Value* sec_modes = root.find("modes");
    if(sec_modes) {
        const toml::Table& modes = sec_modes->as<toml::Table>();


        for(auto& mode : modes) {
            mode_desc new_mode;
            new_mode.name = mode.first;

            for(auto& key_val : mode.second.as<toml::Table>()) {
                const string& key = key_val.first;

                if(key_val.second.is<string>()) {
                    const string& val = key_val.second.as<string>();

                    if(key == "type") {
                        new_mode.type = val;
                    }
                    else {
                        new_mode.values[key] = val;
                    }
                }
                else if(key_val.second.is<toml::Array>()) {
                    const toml::Array& val = key_val.second.as<toml::Array>();

                    for(auto& v : val) {
                        new_mode.lists[key].push_back(v.as<string>());
                    }
                }
            }

            if(!new_mode.type.empty()) {
                res.modes[new_mode.name] = move(new_mode);
            }
        }
    }

    const toml::Value* sec_presets = root.find("presets");
    if(sec_presets) {
        const toml::Table& presets = sec_presets->as<toml::Table>();

        for(auto& preset : presets) {
            preset_desc new_preset;
            new_preset.name = preset.first;

            for(auto& key_val : preset.second.as<toml::Table>()) {
                const string& key = key_val.first;
                const string& val = key_val.second.as<string>();

                if(key == "button_a") {
                    new_preset.button_a = val;
                }
                else if(key == "button_b") {
                    new_preset.button_b = val;
                }
                else if(key == "button_x") {
                    new_preset.button_x = val;
                }
                else if(key == "button_y") {
                    new_preset.button_y = val;
                }
                else if(key == "select") {
                    new_preset.select = val;
                }
                else if(key == "start") {
                    new_preset.start = val;
                }
                else if(key == "steam") {
                    new_preset.steam = val;
                }
                else if(key == "left_shoulder") {
                    new_preset.left_shoulder = val;
                }
                else if(key == "right_shoulder") {
                    new_preset.right_shoulder = val;
                }
                else if(key == "left_gripper") {
                    new_preset.left_gripper = val;
                }
                else if(key == "right_gripper") {
                    new_preset.right_gripper = val;
                }
                else if(key == "left_trigger") {
                    new_preset.left_trigger = val;
                }
                else if(key == "right_trigger") {
                    new_preset.right_trigger = val;
                }
                else if(key == "joystick") {
                    new_preset.joystick = val;
                }
                else if(key == "left_trackpad") {
                    new_preset.left_trackpad = val;
                }
                else if(key == "right_trackpad") {
                    new_preset.right_trackpad = val;
                }
            }

            res.presets[new_preset.name] = move(new_preset);
        }
    }

    return res;
}
