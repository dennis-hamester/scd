// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "mode.hpp"
#include "action.hpp"
#include "modes/button/simple_button_mode.hpp"
#include "modes/joystick/four_buttons_mode.hpp"
#include "modes/joystick/simple_joystick_mode.hpp"
#include "modes/trackpad/circle_mode.hpp"
#include "modes/trackpad/four_buttons_mode.hpp"
#include "modes/trackpad/mouse_mode.hpp"
#include "modes/trackpad/simple_trackpad_mode.hpp"
#include "modes/trigger/simple_trigger_mode.hpp"

using namespace std;

const string& mode::name() const {
    return _name;
}

template <typename T>
static action_list<T> create_actions(const string& slot, const mode_desc& desc, const action_desc_map& actions) {
    action_list<T> res;

    auto slot_iter_str = desc.values.find(slot);
    if(slot_iter_str != desc.values.end()) {
        const string& action_name = slot_iter_str->second;
        auto action_iter = actions.find(action_name);
        if(action_iter != actions.end()) {
            res.push_back(T::create(action_iter->second));
        }

        return res;
    }

    auto slot_iter_list = desc.lists.find(slot);
    if(slot_iter_list != desc.lists.end()) {
        auto& list = slot_iter_list->second;
        for(const string& action_name : list) {
            auto action_iter = actions.find(action_name);
            if(action_iter != actions.end()) {
                res.push_back(T::create(action_iter->second));
            }
        }

        return res;
    }

    return res;
}

action_list<simple_action> mode::create_simple_actions(const string& slot, const mode_desc& desc, const action_desc_map& actions) {
    return create_actions<simple_action>(slot, desc, actions);
}

action_list<axis_action> mode::create_axis_actions(const string& slot, const mode_desc& desc, const action_desc_map& actions) {
    return create_actions<axis_action>(slot, desc, actions);
}

void mode::execute_simple_actions(const action_list<simple_action>& actions, controller& ctrl) {
    for(auto& action : actions) {
        action->execute(ctrl);
    }
}

void mode::execute_axis_actions(const action_list<axis_action>& actions, controller& ctrl, float value) {
    for(auto& action : actions) {
        action->execute(ctrl, value);
    }
}

mode::mode(const string& name)
    : _name(name) {
}

unique_ptr<button_mode> button_mode::create(const mode_desc& desc, const action_desc_map& actions) {
    const string& type = desc.type;

    if(type == "simple") {
        return make_unique<modes::button::simple_button_mode>(desc, actions);
    }

    return nullptr;
}

button_mode::button_mode(const string& name)
    : mode(name) {
}

unique_ptr<trigger_mode> trigger_mode::create(const mode_desc& desc, const action_desc_map& actions) {
    const string& type = desc.type;

    if(type == "simple") {
        return make_unique<modes::trigger::simple_trigger_mode>(desc, actions);
    }

    return nullptr;
}

trigger_mode::trigger_mode(const string& name)
    : mode(name) {
}

unique_ptr<joystick_mode> joystick_mode::create(const mode_desc& desc, const action_desc_map& actions) {
    const string& type = desc.type;

    if(type == "four_buttons") {
        return make_unique<modes::joystick::four_buttons_mode>(desc, actions);
    }
    else if(type == "simple") {
        return make_unique<modes::joystick::simple_joystick_mode>(desc, actions);
    }

    return nullptr;
}

joystick_mode::joystick_mode(const string& name)
    : mode(name) {
}

unique_ptr<trackpad_mode> trackpad_mode::create(const mode_desc& desc, const action_desc_map& actions) {
    const string& type = desc.type;

    if(type == "circle") {
        return make_unique<modes::trackpad::circle_mode>(desc, actions);
    }
    else if(type == "four_buttons") {
        return make_unique<modes::trackpad::four_buttons_mode>(desc, actions);
    }
    else if(type == "mouse") {
        return make_unique<modes::trackpad::mouse_mode>(desc, actions);
    }
    else if(type == "simple") {
        return make_unique<modes::trackpad::simple_trackpad_mode>(desc, actions);
    }

    return nullptr;
}

trackpad_mode::trackpad_mode(const string& name)
    : mode(name) {
}
