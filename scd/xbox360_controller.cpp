// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include "xbox360_controller.hpp"
#include <map>

using namespace std;

static const map<string, xbox360_button> xbox360_button_name_map = {
    { "a",              xbox360_button::btn_a },
    { "b",              xbox360_button::btn_b },
    { "x",              xbox360_button::btn_x },
    { "y",              xbox360_button::btn_y },
    { "left_shoulder",  xbox360_button::btn_left_shoulder },
    { "right_shoulder", xbox360_button::btn_right_shoulder },
    { "select",         xbox360_button::btn_select },
    { "start",          xbox360_button::btn_start },
    { "mode",           xbox360_button::btn_mode },
    { "left_thumb",     xbox360_button::btn_left_thumb },
    { "right_thumb",    xbox360_button::btn_right_thumb },
};

static const map<string, xbox360_axis> xbox360_axis_name_map = {
    { "left_thumb_x",  xbox360_axis::axis_left_thumb_x },
    { "left_thumb_y",  xbox360_axis::axis_left_thumb_y },
    { "right_thumb_x", xbox360_axis::axis_right_thumb_x },
    { "right_thumb_y", xbox360_axis::axis_right_thumb_y },
    { "left_trigger",  xbox360_axis::axis_left_trigger },
    { "right_trigger", xbox360_axis::axis_right_trigger },
    { "dpad_x",        xbox360_axis::axis_dpad_x },
    { "dpad_y",        xbox360_axis::axis_dpad_y },
};

xbox360_button xbox360_button_from_string(const string& name) {
    return xbox360_button_name_map.at(name);
}

xbox360_axis xbox360_axis_from_string(const string& name) {
    return xbox360_axis_name_map.at(name);
}
