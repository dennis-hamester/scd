// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_KEYBOARD_HPP_
#define _SCD_KEYBOARD_HPP_

#include <scd/config.hpp>
#include <string>

enum class keyboard_key {
    key_esc = 0,
    key_1,
    key_2,
    key_3,
    key_4,
    key_5,
    key_6,
    key_7,
    key_8,
    key_9,
    key_0,
    key_minus,
    key_equal,
    key_backspace,
    key_tab,
    key_q,
    key_w,
    key_e,
    key_r,
    key_t,
    key_y,
    key_u,
    key_i,
    key_o,
    key_p,
    key_leftbrace,
    key_rightbrace,
    key_enter,
    key_leftctrl,
    key_a,
    key_s,
    key_d,
    key_f,
    key_g,
    key_h,
    key_j,
    key_k,
    key_l,
    key_semicolon,
    key_apostrophe,
    key_grave,
    key_leftshift,
    key_backslash,
    key_z,
    key_x,
    key_c,
    key_v,
    key_b,
    key_n,
    key_m,
    key_comma,
    key_dot,
    key_slash,
    key_rightshift,
    key_kpasterisk,
    key_leftalt,
    key_space,
    key_capslock,
    key_f1,
    key_f2,
    key_f3,
    key_f4,
    key_f5,
    key_f6,
    key_f7,
    key_f8,
    key_f9,
    key_f10,
    key_numlock,
    key_scrolllock,
    key_kp7,
    key_kp8,
    key_kp9,
    key_kpminus,
    key_kp4,
    key_kp5,
    key_kp6,
    key_kpplus,
    key_kp1,
    key_kp2,
    key_kp3,
    key_kp0,
    key_kpdot,
    key_f11,
    key_f12,
    key_kpenter,
    key_rightctrl,
    key_kpslash,
    key_sysrq,
    key_rightalt,
    key_home,
    key_up,
    key_pageup,
    key_left,
    key_right,
    key_end,
    key_down,
    key_pagedown,
    key_insert,
    key_delete,
    key_pause,
    key_kpcomma,
    key_leftmeta,
    key_rightmeta,
    key_compose,

    key_max // Must be last
};

keyboard_key keyboard_key_from_string(const std::string& name);

#if defined(SCD_OS_LINUX)
#include "linux/keyboard.hpp"
#endif

#endif // _SCD_KEYBOARD_HPP_
