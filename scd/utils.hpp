// Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef _SCD_UTILS_HPP_
#define _SCD_UTILS_HPP_

#include "mouse.hpp"
#include <cmath>
#include <vector>
#include <boost/circular_buffer.hpp>

static const float pi = acos(-1);

static inline float rotate_vec_x(float x, float y, float angle) {
    return x * std::cos(angle) - y * std::sin(angle);
}

static inline float rotate_vec_y(float x, float y, float angle) {
    return x * std::sin(angle) + y * std::cos(angle);
}

static inline float angle_diff(float from, float to) {
    float diff = to - from;
    if(diff > pi) {
        diff -= 2.0f * pi;
    }
    else if(diff <= -pi) {
        diff += 2.0f * pi;
    }

    return diff;
}

static inline bool angle_in_range(float angle, float target, float range) {
    return std::abs(angle_diff(angle, target)) < range;
}

class deadzone_1d {
public:
    deadzone_1d(float percent = 0.0f) {
        set_deadzone(percent);
    }

    void set_deadzone(float percent) {
        _squared_thr = percent * percent / 10000.0f;
    }

    bool operator()(float value) const {
        return (value * value) >= _squared_thr;
    }

private:
    float _squared_thr;
};

class deadzone_2d {
public:
    deadzone_2d(float percent = 0.0f) {
        set_deadzone(percent);
    }

    void set_deadzone(float percent) {
        _squared_thr = percent * percent / 10000.0f;
    }

    bool operator()(float value_a, float value_b) const {
        return ((value_a * value_a) + (value_b * value_b)) >= _squared_thr;
    }

private:
    float _squared_thr;
};

class mouse_move_helper {
public:
    mouse_move_helper(mouse_axis axis)
        : _axis(axis) {
    }

    float get_velocity() const {
        return _velocity;
    }

    void set_velocity(float velocity) {
        if(_velocity == 0.0f) {
            _elapsed = 0.0f;
        }

        _velocity = velocity;
    }

    void update(mouse& ms, uint32_t elapsed) {
        if(_velocity == 0.0f) {
            return;
        }

        _elapsed += elapsed;
        float ms_per_unit = 1000.0f / std::abs(_velocity);
        if(_elapsed < ms_per_unit) {
            return;
        }

        int units = _elapsed / ms_per_unit;
        _elapsed -= ms_per_unit * units;
        if(_velocity < 0) {
            units *= -1;
        }

        ms.move(_axis, units);
    }

private:
    mouse_axis _axis;
    float _velocity = 0.0f;
    float _elapsed = 0.0f;
};

class low_pass_filter {
public:
    low_pass_filter(int taps = 21, float cutoff = 0.05f)
        : _buffer(taps) {
        const float b_alpha = 0.16f;
        const float b_a0 = (1.0f - b_alpha) / 2.0f;
        const float b_a1 = 0.5f;
        const float b_a2 = b_alpha / 2.0f;

        float sum = 1.0f;
        _filter.push_back(1.0f);
        for(int i = 1; i < taps; ++i) {
            float a = 2.0f * cutoff * i;
            float v = sin(a) / a;
            float bw = b_a0 -
                       b_a1 * cos((2.0f * pi * (i + taps - 1)) / (2 * taps - 1)) +
                       b_a2 * cos((4.0f * pi * (i + taps - 1)) / (2 * taps - 1));

            v *= bw;
            sum += v;
            _filter.push_back(v);
        }

        for(int i = 0; i < taps; ++i) {
            _filter[i] /= sum;
        }
    }

    float filter(float value) {
        if(!_buffer.empty()) {
            _buffer.push_front(value);
        }
        else {
            _buffer.assign(_buffer.capacity(), value);
        }

        auto iter_filter = _filter.begin();
        float res = 0.0f;
        for(auto iter = _buffer.begin(); iter != _buffer.end(); ++iter, ++iter_filter) {
            res += *iter * *iter_filter;
        }

        return res;
    }

    void reset() {
        _buffer.clear();
    }

private:
    std::vector<float> _filter;
    boost::circular_buffer<float> _buffer;
};

#endif // _SCD_UTILS_HPP_
