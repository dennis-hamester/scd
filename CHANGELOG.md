scd Changelog
=============

Version 0.2.0, released on 23.04.2016
-------------------------------------
 * Haptic Feedback!
 * Added a new simple action "feedback" for haptic feedback.
 * The axes related modes (joystick/simple, trackpad/mouse, trackpad/simple,
   trigger/simple) gained new configuration options for controlling haptic
   feedback. The other modes use only simple actions and support feedback
   without changes to the code. Please see the README and the bundled configs on
   how it works.
 * The desktop and xbox360 configs have been updated to include haptic feedback
   similar to how Steam does it.
 * Lowered minimum CMake version to 3.2.
 * Added new joystick mode "four_buttons", which is similar to the existing
   trackpad mode.
 * Added new simple action "mouse_axis". It applies a fixed delta to a mouse
   axis.
 * Added new trackpad mode "circle". It fires simple actions when either a
   clockwise or counter-clockwise movement is detected. The bundled "desktop"
   config uses this to implement a circular scroll-wheel on the left trackpad.
 * The forward and back mouse buttons are now called "forward" and "back",
   instead of "extra_2" and "extra_1".
 * The bundled "desktop" config is now feature-complete. It mimics everything
   that Steams default desktop config does, with the exception of the on-screen
   keyboard of course.
 * The X-Box 360 Buttons X and Y are now fixed. They were swapped previously.
 * The trackpad/mouse mode lost its double-tap ability for now. Single taps (as
   used by the desktop config to simulate left clicks) are still supported and
   should also work more reliant now.
 * The trackpad/mouse mode changed several of its default parameters. Please see
   the README. In particular, `sensitivity` values are now much lower (by a
   factor of 1000) due to fixing a related bug.
 * The trackpad/mouse mode parameter `drag` has been renamed to `friction`. The
   semantics did not change.
 * Require `scraw` v0.2.1 and `scrawpp` v0.2.1.

Version 0.1.0, released on 13.04.2016
-------------------------------------
First release
