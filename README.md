Overview
========
`scd`, "Steam Controller Daemon", is a free and open source driver for the Steam
Controller. It's goal is to mimic most functionality of the official Steam
Client. Keep in mind that `scd` is in early development. It will not eat your
controller (I hope), but the functionality is limited.

At the moment, only Linux is supported.


Current Features
================
 * Fully customizable through configuration files
 * Virtual mouse and keyboard
 * Virtual X-Box 360 controller
 * Haptic feedback
 
 
Planned Features
================
 * Better error handling: that should help debugging config files a lot.
 * Modes for the Gyro
 * Force feedback (virtual X-Box 360 controller)
 * Real daemon with a RPC and a tool to control it
 * Actions that change the behavior (*mode*) of the current configuration
 * In general, more actions and modes (several specific ones are already planned
   but not listed here for now)


Usage
=====
First of all, make sure your user account has read and write access to the Steam
controller USB devices under `/dev/bus/usb`. Read and write access to
`/dev/uinput` is also required, because `scd` creates several virtual input
devices for each controller. If your controller works correctly with the
official Steam client, you can assume it will work with `scd` as well. Also,
make sure that Steam is not running while you plug in controllers. Otherwise,
Steam might grab them first. It is however fine to have Steam running while
`scd` runs.

You must provide a path to where your configuration files (`.toml` files) are as
well as the configuration name, which `scd` will use.

For example:

```shell
scd --configs ~/scd_configs --default-config xbox360
```

The parameter `--default-config` refers to the name of the config, as specified
inside the `.toml` file, which is not necessarily the filename.

`scd` prints out the names of all configurations it has found as well as the
configuration it uses when a controller is plugged in.

Keep in mind, that all this will change a lot once `scd` turns more and more
into a real daemon.


Packages
========
* Arch Linux: https://aur.archlinux.org/packages/scd/
* Arch Linux (git version): https://aur.archlinux.org/packages/scd-git/


Compiling
=========
`scd` is built with [CMake](https://cmake.org/) (3.2). Additionally, the
following libraries are required:

  * `scrawpp` (version 0.1.0, only during compile-time)
    https://gitlab.com/dennis-hamester/scrawpp
  * `scraw` (indirectly through `scrawpp`, required at run-time)
    https://gitlab.com/dennis-hamester/scraw
  * `Boost` http://www.boost.org/
  
```shell
mkdir build
cd build
cmake ..
make
make install
```


Controller Configurations
=========================

Overview
--------
`scd` uses `toml` to describe controller configurations. Please check out the
`toml` specification: https://github.com/toml-lang/toml

I also suggest to look at the bundled configurations to get hang of how they
work.

The Steam controller is divided into several components, which are configured
separately by `modes`. Each component has a specific type, which limits the
available `modes`. For example, the left trackpad is a component, for which you
can assign the "four_buttons" `mode`.

Several of these `mode` assignments are grouped together into a `preset`.
Controller configurations can contain multiple of these `presets`. A dedicated
`preset` is used as default, when the configuration is loaded initially.

Each `mode` has several options, depending on the specific `mode`. Some `modes`
allow you to attach `actions` to certain events. The "four_buttons" `mode`
mentioned above, has for example an "up" event, which (you guessed it) invoked
when your thumb is in the upper part of the trackpad. `Actions` come in two
different types, `simple` actions and `axis` actions.

Structure
---------
Each configuration has four top-level tables: `config`, `actions`, `modes`, and
`presets`.

`actions`, `modes`, and the `presets` table each contain sub-tables. For
example, the following snippet defines a `mode` with the name "some_name" and
sets two options. The name can be chosen arbitrarily. `modes` and `actions`
sub-tables require a "type" parameter, that specifies what kind of `action` or
`mode` is defined this table.

```toml
[modes.some_name]
type = "simple"
option1 = "value1"
option2 = "value2"
```

`config` Table
--------------

| Option         | Description                        |
|----------------|------------------------------------|
| name           | Name of the configuration          |
| default_preset | Name of the default preset         |

`modes`
-------
`modes` are grouped by the Steam controller component they can be attached to in
a `preset`. Each type of component defines a set of valid `modes`. When you
define a `mode`, a "type" parameter has to be set, which selects the `mode` from
the set valid `modes`. This is important to remember, because there are for
example multiple different `modes` called "simple", that refer to completely
different things.

Here is as example:

``` toml
[modes.mode1]
type = "simple"
option_a = "value"

[modes.mode2]
type = "simple"
option_b = "value"

[preset.preset1]
button_a = "mode1"
left_trackpad = "mode2"
```

Even though `mode1` and `mode2` look basically identical, they refer to
different types of `modes`, because they are attached to different components in
the `present`.

Whenever `modes` refer to `actions`, you can specify a single `action` or a list
of `actions`.

A list of valid keys, buttons and axes is only available in the source code at
the moment. Refer to [mouse.cpp](scd/mouse.cpp),
[keyboard.cpp](scd/keyboard.cpp) and
[xbox360_controller.cpp](scd/xbox360_controller.cpp). The keyboard names
actually refer to scancodes, which don't know about keyboard layouts. So, "z"
will actually press "y" on German keyboard. This might change in the future.

### Buttons ###

#### Simple ####

| Option  | Type          | Description        |
|---------|---------------|--------------------|
| type    | string        | "simple"           |
| press   | simple action | Button is pressed  |
| release | simple action | Button is released |

### Trackpad ###

#### Simple ####

| Option        | Type          | Description                                                 |
|---------------|---------------|-------------------------------------------------------------|
| type          | string        | "simple"                                                    |
| touch         | simple action | Trackpad is touched                                         |
| touch_release | simple action | Trackpad is not touched anymore                             |
| x             | axis action   | Horizontal movement                                         |
| y             | axis action   | Vertical movement                                           |
| press         | simple action | Trackpad is pressed                                         |
| release       | simple action | Trackpad is released                                        |
| feedback      | simple action | Used for producing haptic feedback                          |
| deadzone      | float         | Deadzone in percent (default 0)                             |
| feedback_grid | float         | Determines feedback rate. Higher => more often (default 10) |

#### Four Buttons ####

| Option        | Type          | Description                                                      |
|---------------|---------------|------------------------------------------------------------------|
| type          | string        | "four_buttons"                                                   |
| up            | simple action | Upper part of trackpad is pressed                                |
| up_release    | simple action | Upper part of trackpad is released                               |
| down          | simple action | Lower part of trackpad is pressed                                |
| down_release  | simple action | Lower part of trackpad is released                               |
| left          | simple action | Left part of trackpad is pressed                                 |
| left_release  | simple action | Left part of trackpad is released                                |
| right         | simple action | Right part of trackpad is pressed                                |
| right_release | simple action | Right part of trackpad is released                               |
| press         | boolean       | "true" requires pressing, "false" just touching (default "true") |
| fan           | float         | Arc in which a button is considered pressed (default 67.5)       |
| deadzone      | float         | Deadzone in percent (default 40)                                 |

The default `fan` parameter will **not** allow multiple button presses at the
same time. If you to have the diagonals active, such that both adjacent buttons
are considered pressed, set a value of 135.

#### Mouse ####

| Option        | Type          | Description                                                               |
|---------------|---------------|---------------------------------------------------------------------------|
| type          | string        | "mouse"                                                                   |
| tap           | simple action | Trackpad was tapped                                                       |
| tap_release   | simple action | Previous tap was released                                                 |
| press         | simple action | Trackpad is pressed                                                       |
| release       | simple action | Trackpad is released                                                      |
| feedback      | simple action | Used for producing haptic feedback                                        |
| friction      | float         | Percent of velocity to lose per millisecond (default 0.5)                 |
| radius        | float         | Radius in percent where movement is recognized (default 90)               |
| sensitivity   | float         | Higher values => faster (default 200)                                     |
| tap_delay     | integer       | Delay in milliseconds between touch and release for tapping (default 150) |
| feedback_grid | float         | Determines feedback rate. Higher => more often (default 10)               |

#### Circle ####

| Option            | Type          | Description                               |
|-------------------|---------------|-------------------------------------------|
| type              | string        | "circle"                                  |
| clockwise         | simple action | Clockwise rotation                        |
| counter_clockwise | simple action | Counter-clockwise rotation                |
| press             | simple action | Trackpad pressed                          |
| release           | simple action | Trackpad released                         |
| step              | float         | Minimum rotation in degrees (default 10°) |
| deadzone          | float         | Deadzone in percent (default 40%)         |
| radius            | float         | Outer radius in percent (default 100%)    |

Be careful if you assign button presses to `clockwise` or `counter_clockwise`,
because there are no "release" events. You have to release the button
immediately by assigning an array of two actions.

### Trigger ###

#### Simple ####

| Option        | Type          | Description                                                |
|---------------|---------------|------------------------------------------------------------|
| type          | string        | "simple"                                                   |
| axis          | axis action   | Movement                                                   |
| press         | simple action | Joystick is pressed                                        |
| release       | simple action | Joystick is released                                       |
| feedback      | simple action | Used for producing haptic feedback                         |
| deadzone      | float         | Deadzone in percent (default 0)                            |
| feedback_grid | float         | Determines feedback rate. Higher => more often (default 3) |

### Joystick ###

#### Simple ####

| Option        | Type          | Description                                                 |
|---------------|---------------|-------------------------------------------------------------|
| type          | string        | "simple"                                                    |
| x             | axis action   | Horizontal movement                                         |
| y             | axis action   | Vertical movement                                           |
| press         | simple action | Joystick is pressed                                         |
| release       | simple action | Joystick is released                                        |
| feedback      | simple action | Used for producing haptic feedback                          |
| deadzone      | float         | Deadzone in percent (default 0)                             |
| feedback_grid | float         | Determines feedback rate. Higher => more often (default 10) |

#### Four Buttons ####

| Option        | Type          | Description                                                |
|---------------|---------------|------------------------------------------------------------|
| type          | string        | "four_buttons"                                             |
| up            | simple action | Upper part of the joystick                                 |
| up_release    | simple action | Upper part of the joystick                                 |
| down          | simple action | Lower part of the joystick                                 |
| down_release  | simple action | Lower part of the joystick                                 |
| left          | simple action | Left part of the joystick                                  |
| left_release  | simple action | Left part of the joystick                                  |
| right         | simple action | Right part of the joystick                                 |
| right_release | simple action | Right part of the joystick                                 |
| press         | simple action | Joystick is pressed                                        |
| release       | simple action | Joystick is released                                       |
| fan           | float         | Arc in which a button is considered pressed (default 67.5) |
| deadzone      | float         | Deadzone in percent (default 40)                           |

The default `fan` parameter will **not** allow multiple button presses at the
same time. If you to have the diagonals active, such that both adjacent buttons
are considered pressed, set a value of 135.

`actions` Table
---------------

### Simple ###

#### Mouse Button ####

| Option | Type   | Description                |
|--------|--------|----------------------------|
| type   | string | "mouse_button"             |
| button | string | Button to press or release |
| state  | string | "press" or "release"       |

#### Mouse Axis ####

| Option | Type   | Description            |
|--------|--------|------------------------|
| type   | string | "mouse_axis"           |
| axis   | string | Axis to move           |
| value  | float  | Relative axis movement |

#### Keyboard Key ####

| Option | Type   | Description             |
|--------|--------|-------------------------|
| type   | string | "keyboard_key"          |
| key    | string | Key to press or release |
| state  | string | "press" or "release"    |

#### X-Box 360 Button ####

| Option | Type   | Description                |
|--------|--------|----------------------------|
| type   | string | "xbox360_button"           |
| key    | string | Button to press or release |
| state  | string | "press" or "release"       |

#### X-Box 360 Axis ####

| Option | Type   | Description             |
|--------|--------|-------------------------|
| type   | string | "xbox360_axis"          |
| axis   | string | Axis to move            |
| value  | float  | Axis destination [-1;1] |

#### Feedback ####

| Option    | Type    | Description           |
|-----------|---------|-----------------------|
| type      | string  | "feedback"            |
| side      | string  | "left" or "right"     |
| amplitude | integer | Amplitude (default 0) |
| period    | integer | Period (default 0)    |
| count     | integer | Count (default 1)     |

### Axis ###

#### Mouse Axis ####

| Option | Type    | Description       |
|--------|---------|-------------------|
| type   | string  | "mouse_axis"      |
| axis   | string  | Axis to move      |
| invert | boolean | "true" or "false" |

#### X-Box 360 Axis ####

| Option | Type    | Description       |
|--------|---------|-------------------|
| type   | string  | "mouse_axis"      |
| axis   | string  | Axis to move      |
| invert | boolean | "true" or "false" |

`presets` Table
---------------

| Option         | Type          |
|----------------|---------------|
| button_a       | button mode   |
| button_b       | button mode   |
| button_x       | button mode   |
| button_y       | button mode   |
| start          | button mode   |
| select         | button mode   |
| steam          | button mode   |
| left_shoulder  | button mode   |
| right_shoulder | button mode   |
| left_gripper   | button mode   |
| right_gripper  | button mode   |
| left_trigger   | trigger mode  |
| right_trigger  | trigger mode  |
| joystick       | joystick mode |
| left_trackpad  | trackpad mode |
| right_trackpad | trackpad mode |


License
=======
`scd` is distributed under the ISC license. This project is not affiliated with
Valve Cooperation.

```
Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```


`scd` uses the `tinytoml` library (https://github.com/mayah/tinytoml):

```
Copyright (c) 2014, MAYAH
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
